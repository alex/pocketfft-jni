#include <jni.h>
#include <cassert>
#include <cstring>
#include "pocketfft/pocketfft.h"

typedef struct {
    int nFFT;
    double *rptr;
    rfft_plan plan;
} PocketFFTState;

extern "C" JNIEXPORT void JNICALL
Java_org_futo_pocketfft_PocketFFT_initState(
    JNIEnv *env,
    jobject thiz,
    jint nFFT
) {
    PocketFFTState *data = (PocketFFTState *)malloc(sizeof(PocketFFTState));
    
    rfft_plan plan = make_rfft_plan(nFFT);

    data->nFFT = nFFT;
    data->plan = plan;
    data->rptr = new double[nFFT + 1];

    jclass cls = env->GetObjectClass(thiz);
    jfieldID structField = env->GetFieldID(cls, "struct", "J");
    env->SetLongField(thiz, structField, (jlong)data);
}

extern "C" JNIEXPORT void JNICALL
Java_org_futo_pocketfft_PocketFFT_freeState(
    JNIEnv *env,
    jobject thiz
) {
    jclass cls = env->GetObjectClass(thiz);
    jfieldID structField = env->GetFieldID(cls, "struct", "J");
    PocketFFTState *data = (PocketFFTState *)env->GetLongField(thiz, structField);

    delete[] data->rptr;
    destroy_rfft_plan(data->plan);

    free(data);
}


extern "C" JNIEXPORT void JNICALL
Java_org_futo_pocketfft_PocketFFT_forward(
    JNIEnv* env,
    jobject thiz,
    jdoubleArray inAudio,
    jdoubleArray out
) {
    jclass cls = env->GetObjectClass(thiz);
    jfieldID structField = env->GetFieldID(cls, "struct", "J");
    PocketFFTState *data = (PocketFFTState *)env->GetLongField(thiz, structField);

    size_t doubleArraySize = (size_t)env->GetArrayLength(inAudio);
    assert(doubleArraySize == data->nFFT);

    double *doubles = env->GetDoubleArrayElements(inAudio, NULL);

    memcpy((char *)(data->rptr + 1), doubles, data->nFFT * sizeof(double));

    env->ReleaseDoubleArrayElements(inAudio, doubles, JNI_ABORT);

    int res = rfft_forward(data->plan, data->rptr + 1, 1.0);
    assert(res == 0);
    
    data->rptr[0] = data->rptr[1];
    data->rptr[1] = 0.0;

    size_t outArraySize = (size_t)env->GetArrayLength(out);
    assert(outArraySize == (data->nFFT + 1));

    env->SetDoubleArrayRegion(out, 0, data->nFFT + 1, data->rptr);
}