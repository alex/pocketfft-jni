package org.futo.pocketfft

class PocketFFT(val nFFT: Int) {
    companion object {
        init {
            System.loadLibrary("pocketfft-jni")
        }
    }

    var struct: Long = 0

    private external fun initState(nFFT: Int)
    private external fun freeState()
    
    init {
        initState(nFFT)
    }

    protected fun finalize() {
        freeState()
    }

    external fun forward(inAudio: DoubleArray, out: DoubleArray)
}
